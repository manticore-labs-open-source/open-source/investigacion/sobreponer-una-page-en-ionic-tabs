# Como sobreponer una page en ionic tabs

Cuando se utiliza un proyecto de ionic 3 con tabs y se desea colocar una page

Al momento de cerrar una sesión, nos debe redirigir a la page de 'Login'.

![configuracion](./imagenes/configuracion.jpeg)


![cargando](./imagenes/cargando.jpeg)


![login](./imagenes/login.jpeg)

# ***Código***
Al momento de cerrar sesión, debe ir a la página de 'Login' sin sobreponer a alguno de los tabs.
```
salir(){
    this._authService.logout();
    this.navCtrl.setRoot('LoginPage');
    this._app.getRootNavs()[0].setRoot('LoginPage'); //colocar a la página de login como root
}
```




<a href="https://www.facebook.com/jonathan.parra.56" target="_blank"><img alt="Sígueme en Twitter" height="35" width="35" src="https://sguru.org/wp-content/uploads/2018/02/Facebook-PNG-Image-71244.png" title="Sígueme en Twitter"/> Jonathan Parra </a><br>
<a href="https://www.linkedin.com/in/jonathan-parra-a89a68162/" target="_blank"><img alt="Sígueme en LinkedIn" height="35" width="35" src="https://4.bp.blogspot.com/-0KtSvK3BydE/XCrIzgI3RqI/AAAAAAAAH_w/n_rr5DS92uk9EWEegcxeqAcSkV36OWEOgCLcBGAs/s1600/linkedin.png" title="Sígueme en LinkedIn"/> Jonathan Parra</a><br>
<a href="https://www.instagram.com/Choco_20jp/" target="_blank"><img alt="Sígueme en Instagram" height="35" width="35" src="https://4.bp.blogspot.com/-Ilxti1UuUuI/XCrIy6hBAcI/AAAAAAAAH_k/QV5KbuB9p3QB064J08W2v-YRiuslTZnLgCLcBGAs/s1600/instagram.png" title="Sígueme en Instagram"/> Choco_20jp</a><br>